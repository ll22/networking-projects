// CSE 461 Project 2 HTTP Proxy
var net = require('net');

// Get the command line argument
var args = process.argv.slice(2);
// TODO: check argument valid or not
var proxyPort = args[0];

if (proxyPort != parseInt(proxyPort)) {
    console.log("Usage: ./run <port number>");
    process.exit();
} else {
    var date = (new Date()).toString().split(" ");
    var outdate = date[2] + " " + date[1] + " " + date[4];
    console.log(outdate + " - Proxy listening on 0.0.0.0:" + proxyPort);
}

var DEFAULT_HTTP_PORT = 80;
var DEFAULT_HTTPS_PORT = 443;
var HTTP_END_LINE = "\r\n";
var CONNECTION_CLOSE = "close";
var CONNECTION_TAG = "connection:";
var HOST_TAG = "host:";
var HOST_TAG_CAP = "Host:";
var EMPTY_LINE = "";
var HTTP = "http";
var CONNECT_KEY_WORD = "CONNECT";
var HTTPS_OK_HEADER = "HTTP/1.1 200 OK\r\n\r\n";
var HTTPS_BAD_HEADER = "HTTP/1.1 502 Bad Gateway\r\n\r\n";

var sockserver = net.createServer(function (proxySocket) {
    var serviceSocket;
    var httpsFlag = 0;
    var initialFlag = 1; //first data coming in

    proxySocket.on("error", function (e) {
        if (serviceSocket != undefined) {
            serviceSocket.end();
        }

        httpsFlag = 0;
        initialFlag = 1;
    });


    proxySocket.on("close", function(e) {
        if (serviceSocket != undefined) {
            serviceSocket.end();
        }
        
        httpsFlag = 0;
        initialFlag = 1;
    });
    
    //whenever the proxy receivs any data from the browser
    proxySocket.on("data", function (raw_data) {
        data = raw_data.toString();
        if((initialFlag == 1) || (httpsFlag == 0)){ //first command
            initialFlag = 0;
            //find the port number and hostname of webserver
            var flag = is_HTTPS(data);
            var pn_and_hn = find_port_and_hn(data, flag);
            var webserver_port = pn_and_hn[0];
            var webserver_hn = pn_and_hn[1];

            //create a new socket to connect to the webver
            //serviceSocket = new net.Socket();
            serviceSocket = net.createConnection(webserver_port, webserver_hn);
            if (flag == true) {
                httpsFlag = 1;
                proxySocket.write(new Buffer(HTTPS_OK_HEADER));
            } else {
                modified_data = modify_data(data);
                serviceSocket.write(new Buffer(modified_data));
            }
            //connection handler
            /*serviceSocket.connect( webserver_port, webserver_hn, function() {
                //connection succeeds, and send a OK 200 message back to browser]
                if (flag == true) {
                    httpsFlag = 1;
                    proxySocket.write(new Buffer(HTTPS_OK_HEADER));
                } else {
                    modified_data = modify_data(data);
                    serviceSocket.write(new Buffer(modified_data));
                }
            });*/

            //data handler
            serviceSocket.on('data', function(data_to_browser){
                //blindly forward data to browser
                proxySocket.write(data_to_browser);
            });

            //error handler
            serviceSocket.on('error', function(e){

                //if it is a connection error, don't forget to send Bad Gateway 500 to browser
                proxySocket.write(new Buffer(HTTPS_BAD_HEADER));
            });

            //close handler
            serviceSocket.on('close', function(e){
                proxySocket.end(); //close the connection to browser too.
            });

        } else { //not the first piece of data from browser
           serviceSocket.write(raw_data);
        }
    });
    
});

sockserver.listen(proxyPort);

function is_HTTPS(data) {
    var line_array = data.split(HTTP_END_LINE);
    var verb = (line_array[0]).split(' ')[0];
    return (verb == CONNECT_KEY_WORD);
}

//return return [pn, hn];
function find_port_and_hn(data, HTTPS_flag){
    var line_array = data.split(HTTP_END_LINE);
    
    var date = (new Date()).toString().split(" ");
    var outdate = date[2] + " " + date[1] + " " + date[4];
    console.log(HTTPS_flag);
    var message = line_array[0].split(" ");
    var outmsg = message[0] + " " + message[1];
    console.log(outdate + " - >>> " + outmsg);
    
    var pn = undefined;
    var hn = undefined;
    //try to find port number and hostname on the line with "Host" or "host"
    for(i = 1; i < line_array.length; i++){
        var line = line_array[i];
        var start_index = line.search(/Host:/i); //line with "host" or "Host"
        if( start_index > -1 ){
            //parse the hostname
            line = line.substring(start_index); 
            line = line.replace(/ /g,'') //get rid of whitespace
            line = line.substring("Host:".length); //get rid of the "Host:" or "host"
            if(line.indexOf(":") > -1){
                hn = line.split(":")[0];
                pn = line.split(":")[1];
                pn = parseInt(pn);
                return [pn, hn];
            }else{
                hn = line;
                break; //break for loop
            }

        } //else keep going to the next line
    }
    //try to find port number on the request line (1st line) 
    if(pn == undefined) {
        var request_line = line_array[0];
        request_line = request_line.replace('http:', '');
        if(request_line.indexOf(':') > -1){
            pn = (request_line.split(':')[1]).split(' ')[0];
            pn = parseInt(pn);
        } //else do nothing
    }

    //can not find pn from both lines, use defaut one
    //if(pn != parseInt(pn)){
    if ((pn != parseInt(pn)) || (parseInt(pn) > 65335)) {
        if(HTTPS_flag){
            pn = 443;
        }else{ //http
            pn = 80;
        }
    }
    
    //console.log(pn + " " + hn);
    return [pn, hn];
}


function modify_data(raw_data){
    var areg = /^([A-Z]+)\s([^\s]+)\sHTTP\/(\d\.\d)/;
    var arg = raw_data.match(areg);
    var hostreg = /http\:\/\/[^\/]+/;
    if (arg && arg[1] && arg[2] && arg[3]) {
        var url = arg[2].replace(hostreg, "");
        raw_data = raw_data.replace(arg[2], url);
    }
    
    raw_data = raw_data.replace('HTTP/1.1', 'HTTP/1.0');
    raw_data = raw_data.replace('keep-alive', 'close');
    
    return raw_data;
}