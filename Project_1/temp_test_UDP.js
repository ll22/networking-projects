
var dgram = require('dgram');
var logger = require('./logger'); //for leveled printing
var dns = require('dns');


logger.debugLevel = 'debug';
const LOCAL_HOST = '127.0.0.1';

const SERVER_HOSTNAME = "cse461.cs.washington.edu";

var reg_agent_active_socket = dgram.createSocket('udp4');
logger.log('debug', 'Here 1')

var message = new Buffer(12);

/*
//reg_agent_active_socket.send(message, 0, message.length, 46101, "cse461.cs.washington.edu", function(err, bytes) {
reg_agent_active_socket.send(message, 0, message.length, 33336, "127.0.1", function(err, bytes) {
//reg_agent_active_socket.send(message, reg_server_port, reg_server_IP, function(err, bytes) {
	if (err) throw err;
	logger.log('debug', 'Data sent');
});
*/

reg_agent_active_socket.on('listening', function (){
    var address = reg_agent_active_socket.address();
    var rec_msg = 'Active port received: ' + address.address + ":" + address.port;
    logger.log('info', rec_msg);
});


//event to handle incoming data
reg_agent_active_socket.on('message', function (message, remote) {
	console.log('Received %d bytes from %s:%d\n',
	message.length, remote.address, remote.port);

	console.log(message);
});

reg_agent_active_socket.on('close', function() {
	logger.log('debug', 'reg_agent_active_socket is closed');
	process.exit(-1); //exit the client program
});

logger.log('debug', 'Here 2')
reg_agent_active_socket.bind(33333, LOCAL_HOST);
logger.log('debug', 'Here 3')