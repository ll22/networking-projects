
var dgram = require('dgram');
var logger = require('./logger'); //for leveled printing
var dns = require('dns');


logger.debugLevel = 'debug';

//cse461.cs.washington.edu:46101
const LOCAL_HOST = '127.0.0.1';
const MAGIC_NUM = 0xc461; //16 bits
const SERVER_PORT = 46101;
const SERVER_HOSTNAME = "cse461.cs.washington.edu";

// port numbers are consecutive
const reg_agent_active_pn = 33333;
const reg_agent_passive_pn = reg_agent_active_pn + 1;

const MSG_TYPE_NUM_REG = 0x01;
const MSG_TYPE_NUM_REG_ED = 0x02;
const MSG_TYPE_NUM_FETCH = 0x03;
const MSG_TYPE_NUM_FETCH_RES = 0x04;
const MSG_TYPE_NUM_UNREG = 0x05;
const MSG_TYPE_NUM_PROBE = 0x06;
const MSG_TYPE_NUM_ACK = 0x07;

const TIME_OUT_SECS = 1;
const REREG_TIMEOUT = 60;

var sequence_num = 0; // 8 bits
var registration_server_IP;
var prev_sent_cmd;

var prev_cmd = {name: 'name', msg_type: 0x01}

var registered_services = [];


//class for registered service
function registered_serv(sq, ip, pn, serv_name, serv_data, name_len, server_IP, server_pn){
	this.sequence_num_registered = sq;
	this.ip = ip;
	this.pn = pn;
	this.serv_name = serv_name;
	this.serv_data = serv_data;
	this.name_len = name_len;
	this.timer;

	this.reset_timeout = function(server_IP, server_pn){
		var serv_ip = this.ip;
		var serv_pn = this.pn;
		var serv_data = this.serv_data; 
		var serv_name = this.serv_name;
		var name_len = this.name_len;
		var sq = this.sequence_num_registered;

		this.timer = setTimeout( 
			function(){
				prev_cmd.name = 'Re-register';
				prev_cmd.msg_type = MSG_TYPE_NUM_REG;
				logger.log('debug', 'Timeout! Try to re-register:  ' + serv_ip + ':' + serv_pn);
				//logger.log('debug', 'sq sent for the purpose of re-register is: ' + sq);
				socket_send(1, server_IP, server_pn, encode_register(sq, serv_ip, serv_pn, serv_data, serv_name, name_len));
			},

			REREG_TIMEOUT*1000);
	}

	this.unregister = function(server_IP, server_pn){
		prev_cmd.msg_type = MSG_TYPE_NUM_UNREG;
		prev_cmd.name = 'Unregister';
		socket_send(1, server_IP, server_pn, encode_unregister(this.ip, this.pn) );
	}

	this.clear_timeout = function(){
		clearTimeout(this.timer);
	}

}

var reg_agent_local_IP;

var IP_to_be_registered;
var port_to_be_registered;

var cmd_time_out;

dns.resolve4(SERVER_HOSTNAME, function (err, addresses) {
	registration_server_IP = addresses[0];
	logger.log('debug', 'looked up address is: ' + registration_server_IP);
});

dns.lookup(require('os').hostname(), function (err, add, fam) {
  console.log('Registration agent local IP addr: '+ add);
  reg_agent_local_IP = add;
})

//an object of a service
function Service(service_IP, service_port, service_data, service_name_len, service_name){
	this.sequenc_num = sequenc_num;
	this.service_IP = service_IP;
	this.service_port = service_port;
	this.service_data = service_data;
	this.service_name_len = service_name_len;
	this.service_name = service_name;
	//todo: set a timer for register
}

//every time when the user wants to register a new service, we create an object of that service and
//put that object in the below array. When the confirmation msg comes back, we set a timer on the object 
//so that when the timer fires, the callback function of the timer will be trying to re-register itself. 
//When another confirmation msg comes back, the below array is iterated to find which object in the array 
//was trying to re-register or just register, and reset that object's timer basing on the life time.
var arr_services_to_regster = [];

// initialize the agent 2 sockets
var reg_agent_active_socket = dgram.createSocket('udp4');
var reg_agent_passive_socket = dgram.createSocket('udp4');

reg_agent_active_socket.bind(reg_agent_active_pn);
reg_agent_passive_socket.bind(reg_agent_passive_pn);

//sockets listening functions
reg_agent_active_socket.on('listening', function (){
    var address = reg_agent_active_socket.address();
    var rec_msg = 'Active port received: ' + address.address + ":" + address.port;
    logger.log('info', rec_msg);
});

reg_agent_passive_socket.on('listening', function (){
    var address = reg_agent_passive_socket.address();
    var rec_msg = 'Passive port received: ' + address.address + ":" + address.port;
    logger.log('info', rec_msg);
});

//************* after initialization ************//

function int_to_IP(num) {
    var part1 = num & 255;
    var part2 = ((num >> 8) & 255);
    var part3 = ((num >> 16) & 255);
    var part4 = ((num >> 24) & 255);

    return part4 + "." + part3 + "." + part2 + "." + part1;
}

function IP_to_int(IP_str){
    var d = IP_str.split('.');
    return ((((((+d[0])*256)+(+d[1]))*256)+(+d[2]))*256)+(+d[3]);
}

function check_magic_num_and_sequence_num(msg){
	

	var magic_num = msg.readUInt16BE(0, 2);
	var sq_num = msg.readUInt8(2, 1);
	var msg_type_num = msg.readUInt8(3, 1);
	if(magic_num != MAGIC_NUM){
		logger.log('error', 'Magic number is wrong');
		logger.log('error', 'Received magic number is: ' + magic_num);
		return false;
	}

	if(sq_num != sequence_num){
		logger.log('error', 'Sequence number is wrong');
		logger.log('error', 'Expected sequence number is ' + sequence_num);
		logger.log('error', 'Received sequenc number is: ' + sq_num);
		return false;
	}
	return true;
}

//returns the msg type number in hex
function check_msg_type(msg){
	var msg_type_num = msg.readUInt8(3, 1); //3rd byte

	// 4 possible message types
	if(msg_type_num != MSG_TYPE_NUM_ACK &&
	   msg_type_num != MSG_TYPE_NUM_REG_ED &&
	   msg_type_num != MSG_TYPE_NUM_FETCH_RES &&
	   msg_type_num != MSG_TYPE_NUM_PROBE){

		logger.log('error', 'Msg type number is wrong');
		logger.log('error', 'Received msg type number is: ' + msg_type_num);
		return -1; //error
	}

	return msg_type_num;
}

//event to handle incoming data
reg_agent_active_socket.on('message', function (message, remote) {
	var msg_type = check_msg_type(message);
	//logger.log('debug', 'Received msg type is:' + msg_type);
	clearTimeout(cmd_time_out);
	//decode the received packet basing on the data type
	switch(msg_type){
		case MSG_TYPE_NUM_PROBE:
			break;

		case MSG_TYPE_NUM_ACK:
			decode_ACK();
			return;

		case MSG_TYPE_NUM_FETCH_RES:
			//logger.log('debug', 'Fetch response received!!!!!!!');
			//logger.log('debug', 'Response length is: ' + message.length);
			decode_fetch_response(message);
			return;

		case MSG_TYPE_NUM_REG_ED:
			//logger.log('debug', 'MSG_TYPE_NUM_REG_ED to be implemented...');
			decode_registered(message);
			return;

		default:
			logger.log('error', 'Unknow msg type!');
			return;
	}

});

//event to handle incoming data
reg_agent_passive_socket.on('message', function (message, remote) {
	var msg_type = check_msg_type(message);
	if(msg_type != MSG_TYPE_NUM_PROBE){
		logger.log('error', 'message with unknow msg type received. Expecting only probes.');
		return;
	}
	//probe received
	//sequence number must match the sequence number of the message it is ACK'ing
	var received_sequence_num = message.readUInt8(2, 1); //get sequence number
	logger.log('debug', 'Received sequence number is: '+received_sequence_num);
	socket_send(2, remote.address, remote.port, encode_ACK(received_sequence_num));

	logger.log('debug', 'I\'ve been probed!');

});


// sock == 1: use active port to send 
// sock == 2: use passive port to send
function socket_send(sock, reg_server_IP, reg_server_port, message){
	if(sock == 1){
		reg_agent_active_socket.send(message, 0, message.length, reg_server_port, reg_server_IP, function(err, bytes) {
		    if (err) throw err;
		    set_cmd_time(reg_server_IP, reg_server_port, message);
		});
	}else if(sock == 2){
		reg_agent_passive_socket.send(message, 0, message.length, reg_server_port, reg_server_IP, function(err, bytes) {
		    if (err) throw err;
		});
	}else{
		logger.log('error', 'In socket_send(), wrong sock number!');
	}
}


//close sockets
reg_agent_active_socket.on('close', function() {
	logger.log('debug', 'reg_agent_active_socket is closed');
});


reg_agent_passive_socket.on('close', function() {
	logger.log('debug', 'reg_agent_passive_socket is closed');
});


function check_input_args(arg){
	if(arg.length != 2 && arg.length != 4){ //either no argument or 2 arguments (hostname + host port number)
		console.log('Wrong number of input arguments. Specify the port number and server IP address.');
		console.log('Program exiting...');
		process.exit();
	}

	if(arg.length == 2){ 
		//return default host name and port number
		return [SERVER_HOSTNAME, SERVER_PORT];
	}

	// arg.length == 4
	var host_name = arg.slice(2)[0];
	logger.log('debug', 'hostname is '+host_name)

	var port_num = arg.slice(2)[1];
	if(isNaN(port_num)){
		console.log('Port number has to be a integer');
		console.log('Program exiting...');
		process.exit();
	}

	logger.log('debug', 'port number is '+port_num)
	return [host_name, port_num];
}


/*Execution begins*/
//check input argument, and it returns host name and port number
var hn_and_pn = check_input_args(process.argv);
var reg_service_hn = hn_and_pn[0];
var reg_service_pn = hn_and_pn[1];


function set_cmd_time(reg_server_IP, reg_server_port, message){
	cmd_time_out = setTimeout(function() {
		  //console.log(str1 + " " + str2);
		  logger.log('error', 'Wait response times out! Resend...');
		  socket_send(1, reg_server_IP, reg_server_port, message);
		}, TIME_OUT_SECS*1000);
}



//read from stdin
//event on receiving normal input from stdin, and then sending the data
process.stdin.on('readable', () => {
	//'Enter r(egister), u(nregister), f(etch), p(robe), or q(uit):'
  var chunk = process.stdin.read();
  if (chunk !== null) {
	//chunk is the user input data
	var user_in = chunk.toString().replace('\n', ''); //chunk is buffer
	logger.log('debug', user_in);

	switch(user_in.substr(0, 1) ){
		case 'p':
			sequence_num++;
			var msg = encode_outgoing_msg(MSG_TYPE_NUM_PROBE, []);
			//prev_sent_cmd = MSG_TYPE_NUM_PROBE;
			prev_cmd.name = 'Probe';
			prev_cmd.msg_type = MSG_TYPE_NUM_PROBE;
			socket_send(1, registration_server_IP, reg_service_pn, msg);			
			break;

		case 'f':
			var input_arr = user_in.split(' ');
			if(input_arr.length == 2){ // f <service name>
				sequence_num++; //only increment sequence number when the input args are valid
				var requested_service_name = input_arr[1];
				var msg = encode_outgoing_msg(MSG_TYPE_NUM_FETCH, [requested_service_name]);

				prev_cmd.name = 'Fetch';
				prev_cmd.msg_type = MSG_TYPE_NUM_FETCH;
				socket_send(1, registration_server_IP, reg_service_pn, msg);
				break;

			}else if(input_arr.length == 1){ // f
				sequence_num++; //only increment sequence number when the input args are valid
				var msg = encode_outgoing_msg(MSG_TYPE_NUM_FETCH, []); //give an empty array
				//prev_sent_cmd = MSG_TYPE_NUM_FETCH;
				
				prev_cmd.name = 'Fetch';
				prev_cmd.msg_type = MSG_TYPE_NUM_FETCH; 
				socket_send(1, registration_server_IP, reg_service_pn, msg);
				break;

			}else{ //wrong arguments number
				logger.log('error', 'Wrong number of arguments for fetch')
			}
			break;

		case 'u':
			sequence_num++;
			var input_arr = user_in.split(' ');
			if(input_arr.length != 2){
				logger.log('error', 'Wrong number of arguments for un-registration');
				logger.log('error', 'eg. u 12345');
				break;
			}
			var arg_array = [reg_agent_local_IP, input_arr[1]];
			var msg = encode_outgoing_msg(MSG_TYPE_NUM_UNREG, arg_array);
			//prev_sent_cmd = MSG_TYPE_NUM_UNREG;
			prev_cmd.name = 'Unregister';
			prev_cmd.msg_type = MSG_TYPE_NUM_UNREG;
			socket_send(1, registration_server_IP, reg_service_pn, msg);
			break;

		case 'r':
			sequence_num++;
			var input_arr = user_in.split(' ');
			if(input_arr.length != 4){
				logger.log('error', 'Wrong number of arguments for registration');
				logger.log('error', 'eg. r 12345 3294741601 Agent0');
				break;
			}
			var pn = input_arr[1];
			var serv_data = input_arr[2];
			var serv_name = input_arr[3];
			var arg_array = [reg_agent_local_IP, pn, serv_data, serv_name, serv_name.length];
			var msg = encode_outgoing_msg(MSG_TYPE_NUM_REG, arg_array);
			
			//prev_sent_cmd = MSG_TYPE_NUM_REG;
			prev_cmd.name = 'Register';
			prev_cmd.msg_type = MSG_TYPE_NUM_REG;

			//create a new object representing the registered service
			//registered_serv(sq, ip, pn, serv_name, serv_data, name_len, server_IP, service_pn)
			var serv = new registered_serv(sequence_num, reg_agent_local_IP, pn, serv_name, serv_data, serv_name.length, registration_server_IP, reg_service_pn);
			//console.log('debug', ' '+ serv.ip);
			console.log('serv created is: ' + serv);
			registered_services.push(serv);
			socket_send(1, registration_server_IP, reg_service_pn, msg);


			break;

		case 'q':
			shut_down_service_agent();


	}
  }
});


//event on eof case
process.stdin.on('end', () => {
	shut_down_service_agent();
});


function shut_down_service_agent(){
  logger.log('info', 'eof detected');
  logger.log('info', 'Trying to unregister all registered service...');
  unregister_all();

  //Wait for unregistering to finish
  var temp_timer = setTimeout(
  	function(){
  		reg_agent_active_socket.close();
		logger.log('info', 'reg_agent_active_socket closed');
		reg_agent_passive_socket.close();
		logger.log('info', 'reg_agent_passive_pn closed');
		logger.log('info', 'Registration agent quited');
		process.exit(-1); //exit the client program
  	} , 800);
}

function unregister_all(){
	for(i=0; i< registered_services.length; i++){
		registered_services[i].unregister(registration_server_IP, reg_service_pn);
	}
}

function encode_outgoing_msg(msg_type, arg_array){
	switch(msg_type) {
		case MSG_TYPE_NUM_PROBE:
			return encode_probe();

		case MSG_TYPE_NUM_ACK:
			return encode_ACK();

		case MSG_TYPE_NUM_UNREG:
			return encode_unregister(arg_array[0], arg_array[1]);

		case MSG_TYPE_NUM_FETCH:
			return encode_fetch(arg_array);

		case MSG_TYPE_NUM_REG:
			IP_to_be_registered = arg_array[0];
			port_to_be_registered = arg_array[1];
			return encode_register(sequence_num ,arg_array[0], arg_array[1], arg_array[2], arg_array[3], arg_array[4]);

		default:
			logger.log('error', 'Unknow msg type!');
			return;
	}
}

/*functions to encode different msgs*/

function encode_ACK(seq_num){
	var msg_buff = new Buffer(4); 
	var offset = 0;
	msg_buff.writeUInt16BE(MAGIC_NUM, offset, 2); //magical number
	offset += 2;
	msg_buff.writeUInt8(seq_num, offset, 1); //sequenc_num
	offset += 1;
	msg_buff.writeUInt8(MSG_TYPE_NUM_ACK, offset, 1); //msg type
	return msg_buff;
}

function encode_register(sq, service_IP, service_port, service_data, service_name, service_name_len){
	/*
	logger.log('debug', 'service_IP is: ' + service_IP);
	logger.log('debug', 'service_port is: ' + service_port);
	logger.log('debug', 'service_data is: ' + service_data);
	logger.log('debug', 'service_name_len is: ' + service_name_len);
	logger.log('debug', 'service_name is: ' + service_name);
	*/

	var msg_buff = new Buffer(15 + service_name_len); 
	var offset = 0;
	msg_buff.writeUInt16BE(MAGIC_NUM, offset, 2); //magical number
	offset += 2;

	msg_buff.writeUInt8(sq, offset, 1); //sequenc_num
	offset += 1;

	msg_buff.writeUInt8(MSG_TYPE_NUM_REG, offset, 1); //sequenc_num
	offset += 1;

	IP_int = IP_to_int(service_IP); //convert IP address from a string format to a 32-bit int
	msg_buff.writeUInt32BE(IP_int, offset, 4);
	offset += 4;

	msg_buff.writeUInt16BE(service_port, offset, 2);
	offset += 2;

	msg_buff.writeUInt32BE(service_data, offset, 4);
	offset += 4;

	msg_buff.writeUInt8(service_name_len, offset, 1);
	offset += 1;

	msg_buff.writeUInt8(service_name, offset, service_name_len);

	return msg_buff;
}

function encode_unregister(service_IP, service_port){
	var msg_buff = new Buffer(10); //12 bytes in total
	var offset = 0;
	msg_buff.writeUInt16BE(MAGIC_NUM, offset, 2); //magical number
	offset += 2;

	msg_buff.writeUInt8(sequence_num, offset, 1); //sequenc_num
	offset += 1;

	msg_buff.writeUInt8(MSG_TYPE_NUM_UNREG, offset, 1); //sequenc_num
	offset += 1;

	IP_int = IP_to_int(service_IP); //convert IP address from a string format to a 32-bit int
	msg_buff.writeUInt32BE(IP_int, offset, 4);
	offset += 4;

	msg_buff.writeUInt16BE(service_port, offset, 2);
	offset += 2;
	return msg_buff;
}

function encode_fetch(arg_array){
	if(arg_array.length == 1){ // f <service name>
		var requested_service_name = arg_array[0];
		var service_name_len = requested_service_name.length;
		var msg_buff = new Buffer(5 + service_name_len);
		var offset = 0;
		msg_buff.writeUInt16BE(MAGIC_NUM, offset, 2); //magical number
		offset += 2;
		msg_buff.writeUInt8(sequence_num, offset, 1); //sequenc_num
		offset += 1;
		msg_buff.writeUInt8(MSG_TYPE_NUM_FETCH, offset, 1); //msg type
		offset += 1;
		msg_buff.writeUInt8(service_name_len, offset, 1);
		offset += 1;
		msg_buff.writeUInt8(requested_service_name, offset, service_name_len);
		return msg_buff;

	}else if(arg_array.length == 0){ // f
 		var msg_buff = new Buffer(5);
 		var offset = 0;
		msg_buff.writeUInt16BE(MAGIC_NUM, offset, 2); //magical number
		offset += 2;
		msg_buff.writeUInt8(sequence_num, offset, 1); //sequenc_num
		offset += 1;
		msg_buff.writeUInt8(MSG_TYPE_NUM_FETCH, offset, 1); //msg type
		offset += 1;
		msg_buff.writeUInt8(0, offset, 1); //service_name_len is 0
		return msg_buff;

	}else{ //error case
		logger.log('error', 'Wrong arg_array length in encode_fetch()!');
		return -1;
	}
}

function encode_probe(){
	var msg_buff = new Buffer(4); //12 bytes in total
	var offset = 0;
	msg_buff.writeUInt16BE(MAGIC_NUM, offset, 2); //magical number
	offset += 2;
	msg_buff.writeUInt8(sequence_num, offset, 1); //sequenc_num
	offset += 1;
	msg_buff.writeUInt8(MSG_TYPE_NUM_PROBE, offset, 1); //sequenc_num
	return msg_buff;
}

/***********************functions to decode different msgs**************************/
function decode_ACK(){
	if(prev_cmd.msg_type == MSG_TYPE_NUM_PROBE){
		logger.log('info', 'Probe ACK received successfully');

	}else if(prev_cmd.msg_type == MSG_TYPE_NUM_UNREG){
		logger.log('info', 'Unregister ACK received successfully');

	}else{
		logger.log('error', 'ACK received but not at the right time');
	}
}

//no need for decode probe

//return life time
function decode_registered(msg){
	//Register 192.168.0.105:12345 successful: lifetime = 240
	//var print_msg = 'Register ' + IP_to_be_registered + ':' + port_to_be_registered;
	var print_msg = prev_cmd.name + ' ' + IP_to_be_registered + ':' + port_to_be_registered;
	if(prev_cmd.msg_type == MSG_TYPE_NUM_REG){
		print_msg += ' successful: ';
		print_msg += (' lifetime = ' + msg.readUInt16BE(4, 2) );
		logger.log('debug', print_msg);
		//logger.log('debug', 'life time is: ' + msg.readUInt16BE(4, 2) );
		var sq_in_msg = msg.readUInt8(2, 1);
		//logger.log('debug', 'In decode_registered');
		//logger.log('debug', 'sq_in_msg: ' + sq_in_msg);

		for(i=0; i < registered_services.length; i++){
			//console.log('registered_services[i].sequence_num_registered is: ' + registered_services[i].sequence_num_registered);
			if((registered_services[i]).sequence_num_registered == sq_in_msg){
				(registered_services[i]).reset_timeout(registration_server_IP, 46101);
			}
		}

	}else{
		print_msg += ' failed ';
		logger.log('debug', 'Service registration failed');
	}
}

function decode_fetch_response(msg){
	if(prev_cmd.msg_type =! MSG_TYPE_NUM_FETCH){
		logger.log('debug', 'Fetch failed');
		return;
	}

	//previous cmd check passed
	var offset = 4
	var entry_num = msg.readUInt8(offset, 1);
	offset += 1;

	logger.log('debug', 'entry_num: '+ entry_num);
	for(var i=0; i < entry_num; i++){ //decode each entry
		// service IP (4)  service port (2) service data (4)
		var msg_to_print = '[' + i + ']  ';
		var serv_ip = msg.readUInt32LE(offset, 4);
		offset += 4;
		msg_to_print += int_to_IP(serv_ip);
		msg_to_print += '  '

		var serv_port = msg.readUInt16BE(offset, 2);
		offset += 2;
		msg_to_print += (serv_port + ' ');

		var serv_data = msg.readUInt32LE(offset, 4);
		offset += 4;
		msg_to_print += ('' + serv_data);
		logger.log('debug', msg_to_print);
	}
}

