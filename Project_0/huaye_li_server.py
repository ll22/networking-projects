# Partner B: Thread-Based Server, Not Thread-Based Client
# 1. (done)multiple timers, sys.exit()
# 2. (done)how to exit if nothing running
# 3. (prob done)multi threading is not really correct, how to get all threads to stop working
# 4. file, command line input
# 5. (prob done)locking
# 6. other issues, make sure that all boundary cases are checked
# 7. (done)need to consider lost packets before receiving "GOODBYE" from client
# 8. (done)one thread per client
# 9. random number not generating porperly
# 10. how to terminate
# 11. where to add lock, timer and sendto will conflict
import socket
import threading
import sys
import select
import random
#import signal
from threading import Timer

import os
import logging

class Server():
    MAGIC = 0xC461
    VERSION = 0x01
    HELLO = 0x00
    DATA = 0x01
    ALIVE = 0x02
    GOODBYE = 0x03
        
    def __init__(self, host, port):
        #logging.info('Initializing Server')
        print("Waiting on port " + format(port) + "...")
        self.host = host
        self.port = port
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.sock.bind((self.host, self.port))
        self.lock = threading.Lock()
        self.clients_list = {}
        self.id_list = []
        self.curseq = []
        #self.thread_list = []
        self.timer_list = []
        self.readline = None
        
    def create_client(self, SESSION_ID, SEQUENCE, client, idx):
        #self.lock.acquire()
        timer = None
        idx = len(self.id_list)
        self.curseq.append(0)
        self.clients_list[idx] = client
        self.timer_list.append(timer)
        self.id_list.append(SESSION_ID)
        self.print_msg(SESSION_ID, SEQUENCE, "Session created", 0)
        send_header = self.generateHeader(Server.HELLO, SEQUENCE, SESSION_ID)
        self.curseq[idx] = self.curseq[idx] + 1
        self.sock.sendto(send_header, client)  
        # Set timer
        if (self.timer_list[idx] != None):
            self.timer_list[idx].cancel()
        timer = threading.Timer(10, self.timerHandler, [client, SEQUENCE, SESSION_ID])
        self.timer_list[idx] = timer
        self.timer_list[idx].start()
        #self.lock.release()
        return idx
        
    def delete_client(self, idx):
        if (len(self.id_list) > 0):
            del self.clients_list[idx]
            del self.id_list[idx]
            del self.curseq[idx]
            #if (self.thread_list[idx] != None):
                #self.thread_list[idx].join()
            #del self.thread_list[idx]
            if (self.timer_list[idx] != None):
                self.timer_list[idx].cancel()
            del self.timer_list[idx]
            
    def print_msg(self, SESSION_ID, SEQUENCE, msg, flag):
        if (flag == 0):
            print(format(SESSION_ID, '08x') + " [" + format(SEQUENCE) + "] " + msg)
        else:
            print(format(SESSION_ID, '08x') + " Session closed")
            
    def readlineHandler(self):
        while True:
            line = sys.stdin.readline()
            if line == "q\n":
                self.all_goodbye()
                break
        os._exit(0)
    
    def generateHeader(self, command, seqnum, session_id):
        header = Server.MAGIC << 48 | Server.VERSION << 40 | command << 32 | seqnum << 16 | session_id
        return format(header, 'x')
        
    def timerHandler(self, client, SEQUENCE, SESSION_ID):
        #self.lock.acquire()
        command = Server.GOODBYE
        send_header = self.generateHeader(command, SEQUENCE, SESSION_ID)
        self.sock.sendto(send_header, client)
        self.print_msg(SESSION_ID, 0, "", 1)
        try:
            idx = self.id_list.index(SESSION_ID)
        except ValueError:
            idx = -1
        if idx != -1:
            self.delete_client(idx)
        #self.lock.release()
    
    def talkToClient(self, client, msg, idx, SESSION_ID):
        self.lock.acquire()
        command = int(msg[6:8], 16)
        SEQUENCE = int(msg[8:16], 16)
        #SESSION_ID = int(msg[16:24], 16)
        data = msg[24:len(msg)]

        if command == Server.HELLO:
            if (idx == -1):
                # Add a new client to the list
                idx = self.create_client(SESSION_ID, SEQUENCE, client, idx)
            else:
                # If client send HELLO in the middle of transition
                self.print_msg(SESSION_ID, 0, "", 1)
                self.delete_client(idx)
        elif command == Server.DATA:
            if (idx == -1):
                #???Do I need to print out created???
                self.print_msg(SESSION_ID, SEQUENCE, " Session created", 0)
                self.print_msg(SESSION_ID, 0, "", 1)
                send_header = self.generateHeader(Server.GOODBYE, SEQUENCE, SESSION_ID)
                self.sock.sendto(send_header, client)
            else:
                # Cancel previous timer and set new timer
                if (self.timer_list[idx] != None):
                    self.timer_list[idx].cancel()
                timer = threading.Timer(10, self.timerHandler, [client, SEQUENCE, SESSION_ID])
                self.timer_list[idx] = timer
                self.timer_list[idx].start()
                outmessage = data
                command = Server.ALIVE;

                if (SEQUENCE == (self.curseq[idx]-1)): 
                    outmessage = "Duplicate packet!"
                    self.curseq[idx] = self.curseq[idx] + 1
                elif (SEQUENCE > self.curseq[idx]):
                    while (self.curseq[idx] < SEQUENCE):
                        seqnum = self.curseq[idx]
                        self.print_msg(SESSION_ID, seqnum, "Lost packet!", 0)
                        self.curseq[idx] = self.curseq[idx] + 1

                self.print_msg(SESSION_ID, SEQUENCE, outmessage, 0)
                send_header = self.generateHeader(command, SEQUENCE, SESSION_ID)
                self.curseq[idx] = self.curseq[idx] + 1
                self.sock.sendto(send_header, client)
        else:
            self.print_msg(SESSION_ID, 0, "", 1)
            self.delete_client(idx)
        self.lock.release()
        
    def all_goodbye(self):
        #self.lock.acquire()
        if (len(self.id_list) > 0):
            for id_num in self.id_list:
                try:
                    idx = self.id_list.index(id_num)
                except ValueError:
                    idx = -1
                send_header = self.generateHeader(Server.GOODBYE, self.curseq[idx], id_num)
                self.sock.sendto(send_header, self.clients_list[idx])
        #self.lock.release()

    def client_goodbye(self, client, SEQUENCE, SESSION_ID, idx):
        #self.lock.acquire()
        send_header = self.generateHeader(self.GOODBYE, SEQUENCE, SESSION_ID)
        self.sock.sendto(send_header, client)
        
        
        if (idx != -1):
            if (SEQUENCE > self.curseq[idx]):
                while (self.curseq[idx] < SEQUENCE):
                    seqnum = self.curseq[idx]
                    self.print_msg(SESSION_ID, seqnum, "Lost packet!", 0)
                    self.curseq[idx] = self.curseq[idx] + 1
                self.curseq[idx] = self.curseq[idx] + 1
            self.delete_client(idx)

        self.print_msg(SESSION_ID, SEQUENCE, "GOODBYE from client", 0)
        self.print_msg(SESSION_ID, 0, "", 1)
        #self.lock.release()
        
    def listen_clients(self):
        self.readline = threading.Thread(target=self.readlineHandler, args=())
        self.readline.start()
        try:
            while True:
                msg, client = self.sock.recvfrom(1024)
                # If message doesn't make sense, exit
                # If the server session receives a message for which 
                # there is no transition in its current state???
                if (len(msg) < 24):
                    self.all_goodbye()
                    sys.exit()
                    
                MAGIC = int(msg[0:4], 16)
                VERSION = int(msg[4:6], 16)
                
                if ((MAGIC == Server.MAGIC) & (VERSION == Server.VERSION)):
                    #logging.info('Received data from client %s: %s', client, msg)
                    SESSION_ID = int(msg[16:24], 16)
                    SEQUENCE = int(msg[8:16], 16)
                    command = int(msg[6:8], 16)

                    try:
                        idx = self.id_list.index(SESSION_ID)
                    except ValueError:
                        idx = -1
                        
                    flag = 0
                    if (idx != -1):
                        if (SEQUENCE < self.curseq[idx]-1):
                            flag = 1
                            self.print_msg(SESSION_ID, 0, "", 1)
                            send_header = self.generateHeader(self.GOODBYE, SEQUENCE, SESSION_ID)
                            self.sock.sendto(send_header, client)
                            self.delete_client(idx)
                    else:
                        flag = 0
                    
                    if flag == 0:
                        if command == Server.GOODBYE:
                            self.client_goodbye(client, SEQUENCE, SESSION_ID, idx)
                        else:
                            self.talkToClient(client, msg, idx, SESSION_ID)
                            #if (idx == -1):
                                #thread = None
                                #self.thread_list.append(thread)
                            #else:
                                #if (self.thread_list[idx] != None):
                                    #self.thread_list[idx].join()
                            #thread = threading.Thread(target=self.talkToClient, args=(client, msg, idx, SESSION_ID))
                            #self.thread_list[idx] = thread
                            #thread.start()
                    
        except KeyboardInterrupt:
            sys.exit()

if __name__ == '__main__':
    # Make sure all log messages show up
    logging.getLogger().setLevel(logging.DEBUG)

    #b = Server(sys.argv[1], int(sys.argv[2]))
    server = Server("localhost", int(sys.argv[1]))
    server.listen_clients()