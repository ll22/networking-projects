var PORT = 33333;
var HOST = '127.0.0.1';

var dgram = require('dgram');
var client = dgram.createSocket('udp4');
var fs = require('fs')
var logger = require('./logger'); //for leveled printing
logger.debugLevel = 'debug';
//global variables keeping track of status
const MAGIC_NUM = 0xc461; //16 bits
const VERSION = 1; //8 bits

var wait_to_quit = false;

const UDP_PACKET_MAX_BYTE_SIZE = 5;
const TIME_OUT_SECS = 4;
const HELLO = 0;
const DATA = 1;
const ALIVE = 2;
const GOODBYE = 3;

var command_state = HELLO; //8 bits
//client chooses its value at random when it starts
var sequenc_num = 0; //32 bits
var min_session_ID = 0;
var file_name = null;
var max_session_ID = Math.pow(2,32);
//32 bits
var session_ID = Math.floor(Math.random() * (max_session_ID - min_session_ID + 1)) + min_session_ID;

var last_Packet_sent = true;

//Helper functions
function check_input_args(arg){
	//need exactly 3 arguments. The 3rd one is the port number the server is running on
	if(arg.length != 4 && arg.length != 5){
		console.log('Wrong number of input arguments. Specify the port number and server IP address.');
		console.log('Program exiting...');
		process.exit();
	}

	arg.slice(2)[0];
	var port_num = arg.slice(2)[0];
	if(isNaN(port_num)){
		console.log('Port number has to be a integer');
		console.log('Program exiting...');
		process.exit();
	}

	var file_name = null;
	if(arg.length == 5){
		file_name = arg.slice(2)[2];
	}
	return [port_num, arg.slice(2)[1], file_name];
}


function client_Send(user_input){
	header_buf = make_header(); //header is a Buffer type
	data_buf = new Buffer(user_input);
	message = Buffer.concat([header_buf, data_buf]);
	client.send(message, 0, message.length, port_num, server_IP, function(err, bytes) {
	    if (err) throw err;
	    logger.log('debug', 'UDP message sent to ' + server_IP +':'+ port_num);

	});
}

function client_send_file_content(content, index){
	if(content == undefined){ //base case
		return;
	}else{
		header_buf = make_header(); //header is a Buffer type
		data_buf = new Buffer(content);
		message = Buffer.concat([header_buf, data_buf]);
		client.send(message, 0, message.length, port_num, server_IP, function(err, bytes) {
		    if (err) throw err;
		    logger.log('debug', 'UDP message sent to ' + server_IP +':'+ port_num);
		    logger.log('debug', 'Sent line: '+content+'\n');
		    index++;
		    client_send_file_content(content_array[index], index);
		});
	}
}

//update the header information everytime after a msg is sent
function make_header(){
	//try to use buffer
	header_buff = new Buffer(12); //12 bytes in total
	//buf.writeUIntBE(value, offset, byteLength[, noAssert])
	var offset = 0;
	header_buff.writeUInt16BE(MAGIC_NUM, offset, 2); //magical number
	offset += 2;
	header_buff.writeUInt8(VERSION, offset, 1); //version number
	offset += 1;
	header_buff.writeUInt8(command_state, offset, 1);
	if(command_state == HELLO){ //only send HELLO for the first time a msg is sent
		command_state = DATA;
	}

	offset += 1;
	header_buff.writeUInt32BE(sequenc_num, offset, 4); //32 bits
	sequenc_num++; //update sequence_num
	offset += 4;
	header_buff.writeUInt32BE(session_ID, offset, 4); //32 bits

	return header_buff;
}


//return [magic_num, version_num, command_state, sequence_num, session_ID];
function reorganize_buf(message_buf){
	var magic_num = message_buf.readUInt16BE(0, 2);
	var version_num = message_buf.readUInt8(2, 1);
	var command_state = message_buf.readUInt8(3, 1);
	var sequence_num = message_buf.readUInt32BE(4, 4);
	var session_ID = message_buf.readUInt32BE(8, 4);
	return [magic_num, version_num, command_state, sequence_num, session_ID];
}

/*
Execution begins
*/
//check input argument
var connect_info = check_input_args(process.argv);
port_num = connect_info[0];
server_IP = connect_info[1];
if(server_IP == 'localhost'){
	server_IP = '127.0.1';
}

file_name = connect_info[2];

//logger.log('debug', file_name);
client_Send('');

var goodbye_timeout;
var hello_timeout = setTimeout(function() {
  logger.log('error', 'Client times out! 1');
  command_state = GOODBYE;
  client_Send('');
  logger.log('debug', 'GOODBYE sent to server.');
	  goodbye_timeout = setTimeout(function() { 
	  	logger.log('Client is closing...');
	  	client.close(); process.exit(-1); 
	  }, TIME_OUT_SECS*1000);

}, TIME_OUT_SECS*1000);


var content_array;

if(file_name != null){ //there is a input file needs to be transfer over
	//read in the file data
	file_content = fs.readFileSync(file_name, 'utf8');
	//logger.log('debug', 'File content is:');
	//logger.log('debug', file_content);
	//chop off the file data into packets, and send one UDP packet at a time until they are all sent
	content_array = file_content.split('\n');
	var index = 0;
	client_send_file_content(content_array[index], index); //content_array is a global variable
	logger.log('debug', 'Filed finished  sending');
    /*
    for(i=0; i<content_array.length; i++){
    	logger.log('debug', content_array[i])
    	client_Send(content_array[i]);
    }
    */

}

//event on receiving normal input from stdin, and then sending the data
process.stdin.on('readable', () => {
  var chunk = process.stdin.read();
  if (chunk !== null) {
	//process.stdout.write(`sent data: ${chunk}`);
	//chunk is the user input data
	var content = chunk.toString(); //chunk is buffer
	//console.log(content);
	content_array = content.split('\n');
	if(content_array.length == 2){ //just a single command
		content_array = content_array.slice(0,1);
	}
    //logger.log('debug', content);
    //var index = 0;
	//client_send_file_content(content_array[index], index); //content_array is a global variable
    var line;
    for(i=0; i<content_array.length; i++){
    	logger.log('debug', content_array[i])
    	line = content_array[i];
    	client_Send(line);
    }


    //client_Send(chunk);
    
    //set a timeout timer whenever something is sent to the server
	/*
	timeOut = setTimeout(function() {
	  logger.log('error', 'Client times out! 2');
	  command_state = GOODBYE;
	  client_Send('');
	  logger.log('debug', 'GOODBYE sent to server.');
	  wait_to_quit = true;

	}, TIME_OUT_SECS*1000);
	*/
  }
});


//event on eof case
process.stdin.on('end', () => {
  logger.log('debug', 'eof detected');
  command_state = GOODBYE;
  client_Send('');
  logger.log('debug', 'GOODBYE sent to server.');
  //set goodbye timer
  goodbye_timeout = setTimeout(function() { 
	  	logger.log('Client is closing...');
	  	client.close(); process.exit(-1); 
	  }, TIME_OUT_SECS*1000);
});


client.on('listening', function () {
    var address = client.address();
    var initial_msg = 'UDP client listening on ' + address.address + ":" + address.port;
    logger.log('info', initial_msg);
});

client.on('message', function (message, remote) {
	message_buf = new Buffer(message); //convert message into a buffer format

    header = reorganize_buf(message_buf); //[magic_num, version_num, command_state, sequence_num, session_ID];
    if(header[2] == ALIVE){
    	clearTimeout(goodbye_timeout);
    	//logger.log('debug', 'ALIVE is received from the server!');
    }else if(header[2] == HELLO){
    	logger.log('debug', 'HELLO is received from the server!');
    	clearTimeout(hello_timeout);

    }else if(header[2] == GOODBYE){
    	logger.log('debug', 'GOODBYE is received from the server! Client is terminating...');
    	client.close();
    	process.exit(-1); //exit the client program

    }else{
    	logger.log('error', 'Something is received from the server! Session is terminating...');
    	client.close();
    	process.exit(-1); //exit the client program

    }
    //clearTimeout(goodbye_timeout);
});

client.on('close', function() {
	//console.log('Client is closed');
	logger.log('debug', 'Client is closed');
	wait_to_quit = true;
	//process.exit(-1); //exit the client program
});
