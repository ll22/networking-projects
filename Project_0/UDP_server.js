//Author: Lincong Li

var HOST = '127.0.0.1';
var wait_to_quit = false;
//server states
const RECEIVE = 'Receive';
const DONE = 'Done';
const MAGIC_NUM = 0xc461;
const VERSION = 1;

const HELLO = 0;
const DATA = 1;
const ALIVE = 2;
const GOODBYE = 3;

const TIME_OUT_SECS = 3;

var server_shutdown = false;
var all_sessions_length = 0;
var all_sessions = {}//new Array();

//define a session class
function Session (session_ID, client_IP, client_port) {
    this.session_ID = session_ID;
    //this.state = receive;
    this.sequence_num = 0;
    this.IP_address = client_IP;
    this.port_number = client_port;
    //timer for each session
    this.time_out = setTimeout(function() {
	  //console.log(str1 + " " + str2);
	  logger.log('error', 'Client times out! Send GOODBYE to it! ');
	  server_Send(GOODBYE, client_IP, client_port);
	  //delete all_sessions[session_ID];
	  all_sessions_length--;
	  logger.log('info', 'all_sessions_length: '+all_sessions_length);

	}, TIME_OUT_SECS*1000);

	this.reset_timeout = function(session_ID, IP_addr, port){
		clearTimeout(this.time_out); //clear current time out
	    this.time_out = setTimeout(function() { //reset timeout
		  //console.log(str1 + " " + str2);
		  logger.log('error', 'Client times out! Send GOODBYE to it! rest_');
	  	  server_Send(GOODBYE, IP_addr, port);
	  	  //delete all_sessions[session_ID];
	  	  logger.log('info', 'all_sessions_length: '+all_sessions_length);
		}, TIME_OUT_SECS*1000);

		logger.log('debug', 'timeout reseted');
	}

	//used when it sends GOOBYE to client
	this.clear_timeout = function(){
		clearTimeout(this.time_out); //clear current time out
	}

    this.get_ID = function() {
        return this.session_ID;
    };

    this.get_sequence_num = function(){
    	return this.sequence_num;
    }

    this.get_port_num = function(){
    	return this.port_number;
    }

	this.get_IP_address = function(){
    	return this.IP_address;
    }

    this.set_sequence_num = function(new_sequence_num){
    	this.sequence_num = new_sequence_num;
    }

    this.print_session = function(){
    	console.log('ID:');
    	console.log(this.session_ID);
    	console.log('sequence_num:');
    	console.log(this.sequence_num);

    }

}


//check input arguments
function check_input_args(arg){
	//need exactly 3 arguments. The 3rd one is the port number the server is running on
	if(arg.length != 3){
		//console.log('Wrong number of input arguments. Only need to specifies the port number.')
		console.log('Wrong number of input arguments. Only need to specifies the port number.');
		console.log('Program exiting...');
		process.exit();
	}

	arg.slice(2)[0];
	var port_num = arg.slice(2)[0];
	if(isNaN(port_num)){
		console.log('Port number has to be a integer');
		console.log('Program exiting...');
		process.exit();
	}
	return port_num;
}


function check_magic_and_version_num(message_buf){

	if(message_buf[0] != MAGIC_NUM || message_buf[1] != VERSION){
		return false;
	}else{
		return true; //testing
	}
}

//return [magic_num, version_num, command_state, sequence_num, session_ID];
function reorganize_buf(message_buf){
	var magic_num = message_buf.readUInt16BE(0, 2);
	var version_num = message_buf.readUInt8(2, 1);
	var command_state = message_buf.readUInt8(3, 1);
	var sequence_num = message_buf.readUInt32BE(4, 4);
	var session_ID = message_buf.readUInt32BE(8, 4);
	/*
	console.log(magic_num);
	console.log(version_num);
	console.log(command_state);
	console.log(sequence_num);
	console.log(session_ID);
	*/
	return [magic_num, version_num, command_state, sequence_num, session_ID];
}


function check_valid_new_client(header){

}

function print_all_sessions(as){
	var session_count = 0;
    for (var k in as) {
        if (as.hasOwnProperty(k)) {
           session_count++;
           console.log('session '+session_count.toString() + ' info:');
           as[k].print_session();
        }
    }
    if(session_count == 0){
    	console.log('No session.');
    }
}

//return true if it is a new session, and false otherwise
function is_new_client(header){
	var cur_session_ID = header[4];
	//search the list of existing sessions
	//for(i=0; i<all_sessions.length; i++){
	//	if(all_sessions[i].get_ID == cur_session_ID){
	//		return false;
	//	}
	//}
	//return true;
	return (all_sessions[cur_session_ID] == undefined);

}

function server_Send(command_state, client_IP, client_port){
	header_buf = make_header(command_state); //header is a Buffer type
	// = new Buffer(user_input);
	//message = Buffer.concat([header_buf, data_buf]);
	message = header_buf;
	//console.log('In client_Send, message is:');
	//console.log(message);
	server.send(message, 0, message.length, client_port, client_IP, function(err, bytes) {
	    if (err) throw err;

	    if(server_shutdown == true && all_sessions_length > 0){
	    	all_sessions_length--;
	    }

	    logger.log('debug', 'UDP message sent to ' + client_IP +':'+ client_port);
	    
	    //Only quit the server when all sessions are gone, and the server_shutdown flag is set
	    if(server_shutdown == true && all_sessions_length == 0){
	    	
	    	server.close();
	    }
	});
}

function make_header(command){
	//try to use buffer
	header_buff = new Buffer(12); //12 bytes in total
	//buf.writeUIntBE(value, offset, byteLength[, noAssert])
	var offset = 0;
	header_buff.writeUInt16BE(MAGIC_NUM, offset, 2); //magical number
	offset += 2;
	header_buff.writeUInt8(VERSION, offset, 1); //version number
	offset += 1;
	header_buff.writeUInt8(command, offset, 1);
	offset += 1;
	header_buff.writeUInt32BE(0, offset, 4); //32 bits sequence_num = 0
	offset += 4;
	header_buff.writeUInt32BE(0, offset, 4); //32 bits sequence_ID = 0

	return header_buff;
}

function goodbye_to_all_clients(){
    for (var k in all_sessions) {
        if (all_sessions.hasOwnProperty(k)) {
           var client_IP = all_sessions[k].get_IP_address();
           var client_p_num = all_sessions[k].get_port_num();
           logger.log('debug','Server sent GOODBYE to '+client_IP+':'+client_p_num);
           server_Send(GOODBYE, client_IP, client_p_num);
        }
    }
}

function quit_server(){
  if(all_sessions_length == 0){
  	logger.log('debug', 'No session to say GOODBYE');
  	server.close();
  }
  goodbye_to_all_clients();

  //delay for a little

  server_shutdown = true;
}
//function 

/*
Execution begins
*/
//check input argument
var port_num = check_input_args(process.argv);

//import UDP module and create a socket with a IPv4 address
var dgram = require('dgram');
var logger = require('./logger'); //for leveled printing
logger.debugLevel = 'info';

var server = dgram.createSocket('udp4');


//event on receiving normal input from stdin, and then sending the data
process.stdin.on('readable', () => {
  var chunk = process.stdin.read();
  if (chunk !== null) {
  	if(chunk == 'q\n'){
  		logger.log('debug', 'quit key detected. Server is quiting...');
  		quit_server();
  	}else{
	    logger.log('debug', 'Don\'t enter anything except \'q\' or \'eof\' to quit server!');
	}
  }
});


//event on eof case
process.stdin.on('end', () => {
  //console.log('eof detected');
  logger.log('debug', 'eof detected. Server is quiting...');

  quit_server();
});


server.on('listening', function () {
    var address = server.address();
    var initial_msg = 'UDP Server listening on ' + address.address + ":" + address.port;
    logger.log('info', initial_msg);
});


//event to handle incoming data
server.on('message', function (message, remote) {
	message_buf = new Buffer(message); //convert message into a buffer format
    header = reorganize_buf(message_buf); //[magic_num, version_num, command_state, sequence_num, session_ID];

    //check magic number
    if(!check_magic_and_version_num(header)){ //if either the magic number and version number is wrong
    	logger.log('error', 'Either the magic number or the version number is wrong.');
    	logger.log('error', 'Received magic number is: '+(header[0]).toString(16));
    	logger.log('error', 'Correct magic number is: '+(0xc461).toString(16));
    	logger.log('error', 'Version number is:' + header[1]);
    	return; //don't do anything
    }

    //check if it is a new session (client)
    if(is_new_client(header)){ //if it is a new session
    	if(header[2] == HELLO){
    		//add to the list of sessions
	    	new_session = new Session(header[4], remote.address, remote.port);
	    	all_sessions[new_session.get_ID()] = new_session;
	    	all_sessions_length++;
	    	logger.log('info', new_session.get_ID().toString(16)+' [' 
	    		+new_session.get_sequence_num().toString()+'] '+'Session created');
	    	server_Send(HELLO, new_session.get_IP_address(), new_session.get_port_num()); //send ALIVE back to client
    	}
    	//if command state is not HELLO, terminate


    }else{ //not a new session

    	//if it is a valid session
    	all_sessions[header[4]].reset_timeout(header[4], all_sessions[header[4]].get_IP_address(), all_sessions[header[4]].get_port_num());
    	var header_to_print = header[4].toString(16) + ' ['+header[3].toString()+'] '
    	if(header[2] == DATA){
	    	//checklist: 1.the command state 2.sequence number
	    	//send ACK to client
	    	//print data
	    	//console.log('Current sequence number is:'+header[3]);
	    	logger.log('debug', 'Current sequence number is:'+header[3]);
	    	//find the previous sequence number
	    	var temp_session = all_sessions[header[4]];
	    	var prev_sequence_num = temp_session.get_sequence_num();

	    	var sequence_num_dif = header[3] - prev_sequence_num;
	    	//update session number
	    	temp_session.set_sequence_num(header[3]); //set the sequence number to be the current one
	    	all_sessions[header[4]] = temp_session; //set it back

	    	//all_sessions[header[4]] = all_sessions[header[4]]).set_sequence_num() = header[3];
	    	if(sequence_num_dif < 0 ){
	    		//protocal error. Close session
	    		//console.log('Protocal error!');
	    		logger.log('error', header_to_print+'Protocal error!');

	    	}else if(sequence_num_dif == 0){
	    		//duplicated packet
	    		//console.log('Duplicated packet!');
	    		logger.log('error', header_to_print+'Duplicated packet!');

	    	}else if(sequence_num_dif == 1){
	    		//correct case. Print data part of the msg to stdout
	    		//console.log(remote.address + ':' + remote.port + ':');
	    		logger.log('debug', remote.address + ':' + remote.port + ':');
	    		logger.log('debug', 'received data: '+(message_buf.toString()).substr(12));
	    		server_Send(ALIVE, remote.address, remote.port); //send ALIVE back to client
	    		logger.log('info', header_to_print+(message_buf.toString()).substr(12) );

	    	}else{ //sequence_num_dif > 1
	    		for(i=0; i<sequence_num_dif; i++){
	    			logger.log('error', header_to_print+'Packet lost!');
	    		}
	    	}

    	}else if(header[2] == GOODBYE){
    		//send a GOODBYE to the client, and remove its session from the list

    		logger.log('debug', 'GOODBY received. Deleted session ' + header[4]);
    		logger.log('info', header_to_print+'GOODBYE received from client');
    		server_Send(GOODBYE, remote.address, remote.port);
    		all_sessions[header[4]].clear_timeout();
    		delete all_sessions[header[4]];
    		all_sessions_length--;

    	}else{ //not a valid session, discard that session, HELLO received again. WRONG!
    		logger.log('debug', 'HELLO received again in the Recieve state');

    		logger.log('error', header_to_print+'HELLO received again. Server is terminating...');
  			goodbye_to_all_clients();
  			server_shutdown = true;
    	}
    }
    //print_all_sessions(all_sessions);
});

server.on('close', function() {
	logger.log('debug', 'Sever is closed');
	process.exit(-1); //exit the client program
});

server.bind(port_num, HOST);