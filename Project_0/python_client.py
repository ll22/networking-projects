#!/usr/local/bin/python

import threading
import thread
import random
# import signal
from threading import Timer
import time
import Queue
import socket
import math
import sys
from struct import *
import logging
import sys, select

input_data = 0

logging.basicConfig(format='%(levelname)s:%(message)s', level=logging.DEBUG)

class Client():
    MAGIC = 0xC461
    VERSION = 0x01
    HELLO = 0x00
    DATA = 0x01
    ALIVE = 0x02
    GOODBYE = 0x03

    def __init__(self, host, port):
        self.host = host
        self.port = port
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.sock.bind(('', 0))  # OS will then pick an available port
        self.sequence_num = 0
        self.session_id = random.randint(1, int(math.pow(2, 32)))
        self.user_input = ''
        self.command = self.HELLO
        self.total_thread_num = 0
        self.client_continue = True
        self.header_fmt = '!HBBLL'
        # self.sock.bind((self.host, self.port))


    def change_command(self, new_command):
        if new_command not in [self.HELLO, self.DATA, self.GOODBYE]:
            logging.error(new_command + ' is not a wrong command state!')
            logging.error('Client is terminating...')
            exit(-1)
        else:
            self.command = new_command

    def client_send(self, data=None):
        header = pack(self.header_fmt,  Client.MAGIC, Client.VERSION, self.command, self.sequence_num, self.session_id)
        if data is not None:
            msg = header + data
        else:
            msg = header # no data

        self.sock.sendto(msg, (self.host, self.port))
        self.sock.settimeout(4) # set a timer on the socket everytime after something was sent to the server

        if data is not None:
            logging.debug('sent: '+data)
        else:
            logging.debug('sent: '+ str(self.command) )
        self.sequence_num += 1


    def client_read_stdin(self):
        self.total_thread_num += 1

        while self.client_continue is True:

            #try:
                #input_data = raw_input()
            timeout = 0.2
            i, o, e = select.select( [sys.stdin], [], [], timeout)

            if i:  # something was entered by the user
                input_data = sys.stdin.readline().strip()
            else:  # time out

                continue

            if input_data:

                if input_data == 'q':
                    logging.debug('\'q\' detected')
                    self.change_command(client.GOODBYE)
                    client.client_send('')
                    self.client_continue = False
                else:
                     self.client_send(input_data, )

            elif input_data is None:
                print 'EOF detected!'
                self.change_command(client.GOODBYE)
                client.client_send('')
                self.client_continue = False
                print 'No data'
            '''
            else:
                print 'EOF detected!'
                self.change_command(client.GOODBYE)
                client.client_send('')
                self.client_continue = False
            '''

        self.total_thread_num -= 1


    def client_listen(self):
        self.total_thread_num += 1
        while self.client_continue is True:

            try:
                data, addr = self.sock.recvfrom(1024) # buffer size is 1024 bytes
                data = data[:12]
                header = unpack(self.header_fmt, data);
                if header[2] == self.ALIVE:
                    logging.debug('client received: ALIVE')
                elif header[2] == self.GOODBYE:
                    logging.debug('client received: GOODBYE')
                    self.client_continue = False

                    break
                elif header[2] == self.HELLO:
                    logging.debug('client received: HELLO')

                elif header[2] == self.DATA:
                    logging.debug('client received: DATA')
                else:
                    logging.error('Unknown command!!')

            except: # self.sock.timeout:
                logging.debug('socket waiting to receive times out!')
                self.client_continue = False

        self.total_thread_num -= 1
        print 'client_listen thread is quiting...'


q = Queue.Queue()

def quick_test(client):
    client.client_send()
    client.change_command(client.DATA)
    time.sleep(0.5)
    client.client_send()
    client.change_command(client.GOODBYE)
    time.sleep(0.5)
    client.client_send()



if __name__ == "__main__":
    client = Client(sys.argv[1], int(sys.argv[2]))
    logging.critical('Client starts running!') # system info
    client.client_send() # Say hello to server
    client.change_command(client.DATA)

    #quick_test(client)

    t_stdin = threading.Thread(target=client.client_read_stdin, args=())
    t_stdin.daemon = True
    t_stdin.start()

    t_listen = threading.Thread(target=client.client_listen, args=())
    t_listen.daemon = True
    t_listen.start()

    while client.total_thread_num > 0:
        #try:
        time.sleep(0.5)
        #except client.sock.timeout, e:
        #    logging.debug('socket waiting to receive times out!')



    exit()
