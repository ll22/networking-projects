/*
In the node.js intro tutorial (http://nodejs.org/), they show a basic tcp 
server, but for some reason omit a client connecting to it.  I added an 
example at the bottom.
Save the following server in example.js:
*/

var net = require('net');

/*
Defined functions
*/
//check input argument
function check_input_args(arg){
	//need exactly 3 arguments. The 3rd one is the port number the server is running on
	if(arg.length != 3){
		//console.log('Wrong number of input arguments. Only need to specifies the port number.')
		console.log('Wrong number of input arguments. Only need to specifies the port number.');
		console.log('Program exiting...');
		process.exit();
	}

	arg.slice(2)[0];
	var port_num = arg.slice(2)[0];
	if(isNaN(port_num)){
		console.log('Port number has to be a integer');
		console.log('Program exiting...');
		process.exit();
	}
	return port_num;
}

/*
Execution begins
*/
//check input argument
var port_num = check_input_args(process.argv);



//define a server behavior
function server_func(sock) {
	//socket.write('Echo server\r\n');
	//var test = socket.read();
	//socket.pipe(socket);

    // We have a connection - a socket object is assigned to the connection automatically
    console.log('CONNECTED: ' + sock.remoteAddress +':'+ sock.remotePort);

    // Add a 'data' event handler to this instance of socket
    sock.on('data', function(data) {
        
        console.log('DATA ' + sock.remoteAddress + ': ' + sock.remotePort +': ' + data);
        // Write the data back to the socket, the client will receive it as data from the server
        sock.write('You said "' + data + '"');
        
    });
    
    // Add a 'close' event handler to this instance of socket
    sock.on('close', function(data) {
        console.log('CLOSED: ' + sock.remoteAddress +' '+ sock.remotePort);
    });
}

//create a server instance
var server = net.createServer(server_func);

//start server
server.listen(port_num, '127.0.0.1');
