/*
And connect with a tcp client from the command line using netcat, the *nix 
utility for reading and writing across tcp/udp network connections.  I've only 
used it for debugging myself.
$ netcat 127.0.0.1 1337
You should see:
> Echo server
*/

/* Or use this example tcp client written in node.js.  (Originated with 
example code from 
http://www.hacksparrow.com/tcp-socket-programming-in-node-js.html.) */

var net = require('net');


/*
Defined functions
*/
//check input argument
function check_input_args(arg){
	//need exactly 3 arguments. The 3rd one is the port number the server is running on
	if(arg.length != 4){
		console.log('Wrong number of input arguments. Specify the port number and server IP address.');
		console.log('Program exiting...');
		process.exit();
	}

	arg.slice(2)[0];
	var port_num = arg.slice(2)[0];
	if(isNaN(port_num)){
		console.log('Port number has to be a integer');
		console.log('Program exiting...');
		process.exit();
	}

	return [port_num, arg.slice(2)[1]];
}

/*
Execution begins
*/
//check input argument
var connect_info = check_input_args(process.argv);

port_num = connect_info[0];
server_IP = connect_info[1];

//create a socket
var client = new net.Socket();
//connect the socket with a port and an IP address (localhost: '127.0.0.1')
client.connect(port_num, server_IP, function() {
	console.log('Connected');
});

//event on normal input
process.stdin.on('readable', () => {
  var chunk = process.stdin.read();
  if (chunk !== null) {
    process.stdout.write(`sent data: ${chunk}`);
    client.write(chunk);
  }
});

//event on eof case
process.stdin.on('end', () => {
  process.stdout.write('eof detected');
  client.destroy();
});


/*
//event on receiving data
client.on('data', function(data) {
	console.log('Received: ' + data);
	client.destroy(); // kill client after server's response
});
*/

client.on('close', function() {
	console.log('Connection closed');
});
